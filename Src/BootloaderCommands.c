#include "BootloaderCommands.h"

#define BUFFER_SIZE 				15
#define COMMAND_TABLE_SIZE 	50
#define BL_DEBUG_MSG_EN
#define D_UART   						Get_DebugHandle()
//#define C_UART   						Get_BleHandle()

char BLOutput[100];

typedef struct
{
    uint8_t cmd;
    void (*function)(uint8_t *data);
}command_t;


typedef  void (*pFunction)(void);


void commandFwVersion(uint8_t *data);
void commandGetChipId(uint8_t *data);
void commandGoToAddress(uint8_t *data);
void commandFlashErase(uint8_t *data);
void commandMemWrite(uint8_t *data);
void commandMemRead(uint8_t *data);
void commandFlashUnlock(uint8_t *data);
void commandFlashLock(uint8_t *data);
void commandMemWriteFile(uint8_t *data);
void commandSetWRPProtection(uint8_t *data);
void commandReadWRPProtection(uint8_t *data);
void commandSetRDPProtection(uint8_t *data);
void commandReadRDPProtection(uint8_t *data);
void commandDisableSectorProtect(uint8_t *data);
void commandReset(uint8_t *data);
void commandSetMultipleWRPProtection(uint8_t *data);
void commandDisableReadWriteProtect(uint8_t *data);
void (*lets_jump)(void);


command_t const gCommandTable[COMMAND_TABLE_SIZE] =
{
    {0x51, 	commandFwVersion},
    {0x53, 	commandGetChipId},
    {0x55, 	commandGoToAddress},
    {0x56, 	commandFlashErase},
    {0x57, 	commandMemWrite},
    {0x58, 	commandSetWRPProtection},
    {0x59, 	commandMemRead},
    {0x5A, 	commandReadWRPProtection},
		{0x5B, 	commandReadRDPProtection},
		{0x5C, 	commandSetRDPProtection},
		{0x5D, 	commandSetMultipleWRPProtection},
    {0x5E, 	commandDisableSectorProtect},
		{0x5F, 	commandDisableReadWriteProtect},
    {0x60,	commandFlashUnlock},
    {0x61,	commandFlashLock},
    {0x62,	commandMemWriteFile},
    {0x63,	commandReset},
    {0xFF,  NULL},
};


static uint32_t createCrc(uint8_t dataOne, uint8_t dataTwo,
                          uint8_t dataThree, uint8_t dataFour);
static uint8_t bootloaderVerifyAddress(uint32_t go_address);

static uint32_t createCrc(uint8_t dataOne, uint8_t dataTwo,
                          uint8_t dataThree, uint8_t dataFour)
{
    uint32_t newWord = 0;

    newWord = (uint32_t)((unsigned char)(dataFour) << 24 |
                         (unsigned char)(dataThree) << 16 |
                             (unsigned char)(dataTwo) << 8 |
                                 (unsigned char)(dataOne));

		return newWord;
}

//verify the address sent by the host .
static uint8_t bootloaderVerifyAddress(uint32_t go_address)
{
    //so, what are the valid addresses to which we can jump ?
    //can we jump to system memory ? yes
    //can we jump to sram1 memory ?  yes
    //can we jump to sram2 memory ? yes
    //can we jump to backup sram memory ? yes
    //can we jump to peripheral memory ? its possible , but dont allow. so no
    //can we jump to external memory ? yes.

    //incomplete -poorly written .. optimize it
    if (go_address >= SRAM1_BASE && go_address <= SRAM1_END)
    {
        return ADDR_VALID;
    }

    else if ( go_address >= FLASH_BASE && go_address <= FLASH_END)
    {
        return ADDR_VALID;
    }

    else
        return ADDR_INVALID;
}

bool Gpio_ReadUtilSwitch1(void)
{
  bool value = false;

  if(HAL_GPIO_ReadPin(SW_Util1_GPIO_Port,SW_Util1_Pin)==GPIO_PIN_SET)
  {
		value = true;
  }
  return value;
}

bool Gpio_ReadUtilSwitch2(void)
{
  bool value = false;

  if(HAL_GPIO_ReadPin(SW_Util2_GPIO_Port,SW_Util2_Pin)==GPIO_PIN_SET)
  {
		value = true;
  }
  return value;
}

bool Gpio_StartSwitch(void)
{
  bool status = true;
   if(HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_1)==GPIO_PIN_SET)
   {
     status = false;
   }
   return status;

}



bool Parse_Incoming_Command(uint8_t * buffer)
{
    int idx;
    bool valid_cmd_status = false;
    for (idx = 0; gCommandTable[idx].cmd != 0xFF; idx++)
    {
        if (gCommandTable[idx].cmd == buffer[0])
        {
            valid_cmd_status = true;
            break;
        }
    }
    if(valid_cmd_status)
    {
        (*gCommandTable[idx].function)(buffer);
    }

    return valid_cmd_status;
}


void serialPutStr(const char *data)
{
    HAL_UART_Transmit(Get_DebugHandle(),(uint8_t *)data, strlen(data),HAL_MAX_DELAY);
		HAL_UART_Transmit(Get_BleHandle(),(uint8_t *)data, strlen(data),HAL_MAX_DELAY);
}



void commandReset(uint8_t *data)
{
    serialPutStr("Device Reset\n");
		NVIC_SystemReset();
}

void commandFwVersion(uint8_t *data)
{
    uint8_t bootloaderVersion = 0;

    serialPutStr("BOOTLOADER_MSG:CMD_GetFWVersion\n");


    uint32_t hostCrc = createCrc(data[1], data[2], data[3], data[4]);

    uint8_t crcData[4] = {0};
    memcpy(crcData,data,1);

    if (! Bootloader_VerifyCrc(crcData,1,hostCrc))
    {
        serialPutStr("BOOTLOADER_MSG:Checksum success!\n");
        // checksum is correct..
        bootloaderVersion=Bootloader_GetVersion();
				sprintf(BLOutput, "BOOTLOADER_MSG:BL_VER : %d %#x\n",bootloaderVersion,bootloaderVersion);
        serialPutStr(BLOutput);
        serialPutStr("Version 1\n");
    }
    else
    {
        serialPutStr("BOOTLOADER_MSG:checksum fail !!\n");
        //checksum is wrong send nack
        Bootloader_SendNack();
    }
}



void commandGetChipId(uint8_t *data)
{
    uint16_t bootloaderChipId = 0;

    // 1) verify the checksum
    serialPutStr("BOOTLOADER_MSG:CMD_GetChipId\n");


    uint32_t hostCrc = createCrc(data[1], data[2], data[3], data[4]);

    uint8_t crcData[4] = {0};
    memcpy(crcData,data,1);

    if (! Bootloader_VerifyCrc(crcData,1,hostCrc))
    {
        serialPutStr("BOOTLOADER_MSG:checksum success !!\n");

        bootloaderChipId = Bootloader_GetMcuChipId();

				sprintf(BLOutput, "Chip ID %d\n",bootloaderChipId);
				serialPutStr(BLOutput);
    }
    else
    {
        serialPutStr("BOOTLOADER_MSG:checksum fail !!\n");
        //checksum is wrong send nack
        Bootloader_SendNack();
    }
}

void commandGoToAddress(uint8_t *data)
{
    uint32_t GoToAddressValue =0;
    serialPutStr("BOOTLOADER_MSG:CMD_GoToAddress\n");

    uint32_t hostCrc = createCrc(data[1], data[2], data[3], data[4]);


    uint8_t crcData[4] = {0};
    memcpy(crcData,data,1);
    if (! Bootloader_VerifyCrc(crcData,1,hostCrc))
    {
        serialPutStr("BOOTLOADER_MSG:checksum success !!\n");
        GoToAddressValue = (uint32_t)((unsigned char)(data[5]) << 24 |
                                      (unsigned char)(data[6]) << 16 |
                                          (unsigned char)(data[7]) << 8 |
                                              (unsigned char)(data[8]));

				sprintf(BLOutput, "BOOTLOADER_MSG:GO addr: %#x\n",GoToAddressValue);
				serialPutStr(BLOutput);

        if( bootloaderVerifyAddress(GoToAddressValue) == ADDR_VALID )
        {
            /*jump to "go" address.
            we dont care what is being done there.
            host must ensure that valid code is present over there
            Its not the duty of bootloader. so just trust and jump */

            /* Not doing the below line will result in hardfault exception for ARM cortex M */
            //watch : https://www.youtube.com/watch?v=VX_12SjnNhY

            GoToAddressValue += 1; //make T bit =1

            lets_jump = (void(*)(void))((uint8_t *)GoToAddressValue);

            serialPutStr("BOOTLOADER_MSG: jumping to go_address! \n");

            lets_jump();
        }
        else
        {
            serialPutStr("BOOTLOADER_MSG:GO addr invalid ! \n");
        }
    }
    else
    {
        serialPutStr("BOOTLOADER_MSG:checksum fail !!\n");
        //checksum is wrong send nack
        Bootloader_SendNack();
    }
}


void commandFlashErase(uint8_t *data)
{
    uint8_t eraseStatus = 0x00;

    serialPutStr("BOOTLOADER_MSG:CMD_FlashErase\n");

    uint32_t hostCrc = createCrc(data[1], data[2], data[3], data[4]);

    uint8_t crcData[4] = {0};
    memcpy(crcData,data,1);
    if (! Bootloader_VerifyCrc(crcData,1,hostCrc))
    {
        serialPutStr("BOOTLOADER_MSG:checksum success !!\n");
				sprintf(BLOutput, "BOOTLOADER_MSG:Initial Sector: %d\n",data[5]);
				serialPutStr(BLOutput);

				sprintf(BLOutput, "Total Sectors: %d\n",data[6]);
				serialPutStr(BLOutput);

        if(data[5] == 0xFF)
        {
            serialPutStr("Mass Erase!");
        }
        eraseStatus = Bootloader_FlashErase(data[5], data[6]);

				sprintf(BLOutput, "BOOTLOADER_MSG: Flash Erase Status: %#x\n", eraseStatus);
				serialPutStr(BLOutput);
    }
    else
    {
        serialPutStr("BOOTLOADER_MSG:checksum fail !!\n");
        //checksum is wrong send nack
        Bootloader_SendNack();
    }

}

uint8_t Bootloader_FlashErase(uint8_t sector_number ,
                              uint8_t number_of_sector)
{
    //we have totally 8 sectors in mcu .. sector[0 to 7]
    //number_of_sector has to be in the range of 0 to 7
    // if sector_number = 0xff , that means mass erase !
    //Code needs to modified if your MCU supports more flash sectors
    FLASH_EraseInitTypeDef flashErase_handle;
    uint32_t sectorError;
    HAL_StatusTypeDef status;
    uint16_t rdpLevel = 0;
    rdpLevel = Bootloader_GetOptionBytesRDPLevel();
    bool rdpLevelValid = false;

    if(rdpLevel == OB_RDP_LEVEL_2)
    {
        rdpLevelValid = false;
    }
    else if(rdpLevel == OB_RDP_LEVEL_1)
    {
        rdpLevelValid = true;
    }
    else
    {
        rdpLevelValid = true;
    }


    if( number_of_sector > TOTAL_FLASH_SECTORS || (rdpLevel == false))
    {
        return INVALID_SECTOR;
    }

    if( ((sector_number == 0xff ) || (sector_number <= 7)) && rdpLevelValid)
    {
        if(sector_number == (uint8_t) 0xff)
        {
            flashErase_handle.TypeErase = FLASH_TYPEERASE_MASSERASE;
        }
        else
        {
            /*Here we are just calculating how many sectors needs to erased */
            uint8_t remanining_sector = 8 - sector_number;
            if( number_of_sector > remanining_sector)
            {
                number_of_sector = remanining_sector;
            }
            flashErase_handle.TypeErase = FLASH_TYPEERASE_SECTORS;
            flashErase_handle.Sector = sector_number; // this is the initial sector
            flashErase_handle.NbSectors = number_of_sector;
        }
        //flashErase_handle.Banks = FLASH_BANK_1;

        /*Get access to touch the flash registers */
        HAL_FLASH_Unlock();
        flashErase_handle.VoltageRange = FLASH_VOLTAGE_RANGE_3;  // our mcu will work on this voltage range
        status = (uint8_t) HAL_FLASHEx_Erase(&flashErase_handle, &sectorError);
        HAL_FLASH_Lock();

        return status;
    }
    return INVALID_SECTOR;
}



void commandSetWRPProtection(uint8_t *data)
{
    uint8_t status = 0x00;

    serialPutStr("BOOTLOADER_MSG:CMD_SetWriteProtection\n");

    uint32_t hostCrc = createCrc(data[1], data[2], data[3], data[4]);


    uint8_t crcData[4] = {0};
    memcpy(crcData,data,1);
    if (! Bootloader_VerifyCrc(crcData,1,hostCrc))
    {
        serialPutStr("BOOTLOADER_MSG:checksum success !!\n");
			//serialPutStr("BOOTLOADER_MSG:sector : 0x%x, protection_mode: %d\n",data[5],data[6]);
        status = FlashProtectionConfig(data[5], data[6], 0);
				sprintf(BLOutput, "BOOTLOADER_MSG: flash protection enabled (return 0 for true): %#x\n",status);
				serialPutStr(BLOutput);


        if(data[6] == 1)
        {
            serialPutStr("BOOTLOADER_MSG: Write Protection Enabled");
        }
        else if(data[6] == 2)
        {
            serialPutStr("BOOTLOADER_MSG: Read and Write Protection Enabled. Full Chip Erase Required to Change.");
        }
    }
    else
    {
        serialPutStr("BOOTLOADER_MSG:checksum fail !!\n");
        //checksum is wrong send nack
        Bootloader_SendNack();
    }
}


void commandSetMultipleWRPProtection(uint8_t *data)
{

    uint8_t status = 0x00;

    uint32_t hostCrc = createCrc(data[1], data[2], data[3], data[4]);

    uint8_t crcData[4] = {0};
    memcpy(crcData,data,1);
    if (! Bootloader_VerifyCrc(crcData,1,hostCrc))
    {
        status = FlashProtectionConfig(data[5], data[6], 0);
    }
    else
    {
        serialPutStr("BOOTLOADER_MSG:checksum fail !!\n");
        //checksum is wrong send nack
        Bootloader_SendNack();
    }
}

void commandDisableSectorProtect(uint8_t *data)
{
    uint8_t status = 0x00;

    // 1) verify the checksum
    serialPutStr("BOOTLOADER_MSG:Disable Write Protections\n");

    uint32_t hostCrc = createCrc(data[1], data[2], data[3], data[4]);

    uint8_t crcData[4] = {0};
    memcpy(crcData,data,1);
    if (! Bootloader_VerifyCrc(crcData,1,hostCrc))
    {
        serialPutStr("BOOTLOADER_MSG:checksum success !!\n");

        status = FlashProtectionConfig(0,0,1);
				sprintf(BLOutput, "BOOTLOADER_MSG: Return 0 for success: %#x\n",status);
				serialPutStr(BLOutput);
    }
    else
    {
        serialPutStr("BOOTLOADER_MSG:checksum fail !!\n");
        //checksum is wrong send nack
        Bootloader_SendNack();
    }
}

void commandDisableReadWriteProtect(uint8_t *data)
{
    uint8_t eraseStatus = 0x00;

    serialPutStr("BOOTLOADER_MSG:CMD_FlashErase\n");

    uint32_t hostCrc = createCrc(data[1], data[2], data[3], data[4]);

    uint8_t crcData[4] = {0};
    memcpy(crcData,data,1);
    if (! Bootloader_VerifyCrc(crcData,1,hostCrc))
    {
        serialPutStr("BOOTLOADER_MSG:checksum success !!\n");
        serialPutStr("All Sectors Erased\n");
				sprintf(BLOutput, "BOOTLOADER_MSG: Flash Erase Status: %#x\n", eraseStatus);
				serialPutStr(BLOutput);
				eraseStatus = Bootloader_FlashErase(data[5], data[6]);
    }
    else
    {
        serialPutStr("BOOTLOADER_MSG:checksum fail !!\n");
        //checksum is wrong send nack
        Bootloader_SendNack();
    }
}


/*
Modifying user option bytes
To modify the user option value, follow the sequence below:
1. Check that no Flash memory operation is ongoing by checking the BSY bit in the
FLASH_SR register
2. Write the desired option value in the FLASH_OPTCR register.
3. Set the option start bit (OPTSTRT) in the FLASH_OPTCR register
4. Wait for the BSY bit to be cleared.
*/
uint8_t FlashProtectionConfig(uint8_t sectorNum, uint8_t protectionLevel, uint8_t disable)
{
    //First configure the protection mode
    //protection_mode =1 , means write protect of the user flash sectors
    //protection_mode =2, means read/write protect of the user flash sectors
    //According to RM of stm32f446xx TABLE 9, We have to modify the address 0x1FFF C008 bit 15(SPRMOD)

    //Flash option control register (OPTCR)
    volatile uint32_t *pOPTCR = (uint32_t*) 0x40023C14;

    if(disable)
    {
        //disable all r/w protection on sectors

        //Option byte configuration unlock
        HAL_FLASH_OB_Unlock();

        //wait till no active operation on flash
        while(__HAL_FLASH_GET_FLAG(FLASH_FLAG_BSY) != RESET);

        //clear the 31st bit (default state)
        //please refer : Flash option control register (FLASH_OPTCR) in RM
        *pOPTCR &= (~(1 << 31));

        //clear the protection : make all bits belonging to sectors as 1
        *pOPTCR |= (0xFF << 16);

        //Set the option start bit (OPTSTRT) in the FLASH_OPTCR register
        *pOPTCR |= ( 1 << 1);

        //wait till no active operation on flash
        while(__HAL_FLASH_GET_FLAG(FLASH_FLAG_BSY) != RESET);

        HAL_FLASH_OB_Lock();

        return 0;

    }

    if(protectionLevel == (uint8_t) 1)
    {
        //we are putting write protection on the sectors encoded in sector_details argument

        //Option byte configuration unlock
        HAL_FLASH_OB_Unlock();

        //wait till no active operation on flash
        while(__HAL_FLASH_GET_FLAG(FLASH_FLAG_BSY) != RESET);

        //here we are setting just write protection for the sectors
        //clear the 31st bit
        //please refer : Flash option control register (FLASH_OPTCR) in RM
        *pOPTCR &= ~(1 << 31);

        //put write protection on sectors
        *pOPTCR &= ~ (sectorNum << 16);

        //Set the option start bit (OPTSTRT) in the FLASH_OPTCR register
        *pOPTCR |= ( 1 << 1);

        //wait till no active operation on flash
        while(__HAL_FLASH_GET_FLAG(FLASH_FLAG_BSY) != RESET);

        HAL_FLASH_OB_Lock();
    }

    else if (protectionLevel == (uint8_t) 2)
    {
        //Option byte configuration unlock
        HAL_FLASH_OB_Unlock();

        //wait till no active operation on flash
        while(__HAL_FLASH_GET_FLAG(FLASH_FLAG_BSY) != RESET);

        //here wer are setting read and write protection for the sectors
        //set the 31st bit
        //please refer : Flash option control register (FLASH_OPTCR) in RM
        *pOPTCR |= (1 << 31);

        //put read and write protection on sectors
        *pOPTCR &= ~(0xff << 16);
        *pOPTCR |= (sectorNum << 16);

        //Set the option start bit (OPTSTRT) in the FLASH_OPTCR register
        *pOPTCR |= ( 1 << 1);

        //wait till no active operation on flash
        while(__HAL_FLASH_GET_FLAG(FLASH_FLAG_BSY) != RESET);

        HAL_FLASH_OB_Lock();
    }
    return 0;
}

void commandReadWRPProtection(uint8_t *data)
{

    uint8_t status = 0x00;

    // 1) verify the checksum
    serialPutStr("BOOTLOADER_MSG:Get Sector nWRP Protection\n");


    uint32_t hostCrc = createCrc(data[1], data[2], data[3], data[4]);

    uint8_t crcData[4] = {0};
    memcpy(crcData,data,1);
    if (! Bootloader_VerifyCrc(crcData,1,hostCrc))
    {
        serialPutStr("BOOTLOADER_MSG:checksum success !!\n");

        status = Bootloader_GetOptionBytesWRPLevel();

				sprintf(BLOutput, "BOOTLOADER_MSG: Protected Sectors (1 bit for each unprotected sector): %#x\n",status);
				serialPutStr(BLOutput);
    }
    else
    {
        serialPutStr("BOOTLOADER_MSG:checksum fail !!\n");
        //checksum is wrong send nack
        Bootloader_SendNack();
    }
}


uint16_t Bootloader_GetOptionBytesWRPLevel(void)
{
    //This structure is given by ST Flash driver to hold the OB(Option Byte) contents .
    FLASH_OBProgramInitTypeDef OBInit;

    //First unlock the OB(Option Byte) memory access
    HAL_FLASH_OB_Unlock();
    //get the OB configuration details
    HAL_FLASHEx_OBGetConfig(&OBInit);
    //Lock back.
    HAL_FLASH_Lock();

    //We are just interested in r/w protection status of the sectors.
    return (uint16_t)OBInit.WRPSector;
}


void commandReadRDPProtection(uint8_t *data)
{

    uint8_t status = 0x00;

    // 1) verify the checksum
    serialPutStr("BOOTLOADER_MSG:Get Sector RDP Protection\n");

    uint32_t hostCrc = createCrc(data[1], data[2], data[3], data[4]);

    uint8_t crcData[4] = {0};
    memcpy(crcData,data,1);
    if (! Bootloader_VerifyCrc(crcData,1,hostCrc))
    {
        serialPutStr("BOOTLOADER_MSG:checksum success !!\n");

        status = Bootloader_GetOptionBytesRDPLevel();
				sprintf(BLOutput,"BOOTLOADER_MSG: RDP Setting: %#x\n",status);
				serialPutStr(BLOutput);
    }
    else
    {
        serialPutStr("BOOTLOADER_MSG:checksum fail !!\n");
        //checksum is wrong send nack
        Bootloader_SendNack();
    }
}


uint16_t Bootloader_GetOptionBytesRDPLevel(void)
{
    //This structure is given by ST Flash driver to hold the OB(Option Byte) contents .
    FLASH_OBProgramInitTypeDef OBInit;

    //First unlock the OB(Option Byte) memory access
    HAL_FLASH_OB_Unlock();
    //get the OB configuration details
    HAL_FLASHEx_OBGetConfig(&OBInit);
    //Lock back.
    HAL_FLASH_Lock();

    //We are just interested in r/w protection status of the sectors.
    return (uint16_t)OBInit.RDPLevel;
}


void commandSetRDPProtection(uint8_t *data)
{

    uint8_t status = 0x00;

    serialPutStr("BOOTLOADER_MSG:Set RDP Level\n");


    uint32_t hostCrc = createCrc(data[1], data[2], data[3], data[4]);

    uint8_t crcData[4] = {0};
    memcpy(crcData,data,1);
    if (! Bootloader_VerifyCrc(crcData,1,hostCrc))
    {
        serialPutStr("BOOTLOADER_MSG:checksum success !!\n");

        status = Bootloader_GetSetOptionBytesRDPLevel(data[5]);

				sprintf(BLOutput, "BOOTLOADER_MSG: RDP Set: %#x\n",status);
				serialPutStr(BLOutput);
    }
    else
    {
        serialPutStr("BOOTLOADER_MSG:checksum fail !!\n");
        //checksum is wrong send nack
        Bootloader_SendNack();
    }
}

bool Bootloader_GetSetOptionBytesRDPLevel(uint8_t protectionValue)
{
    uint16_t protectionLevel = 0;
    protectionLevel = Bootloader_GetOptionBytesRDPLevel();
		sprintf(BLOutput, "Read Protection Set: %x\n", protectionLevel);
		serialPutStr(BLOutput);

    //This structure is given by ST Flash driver to hold the OB(Option Byte) contents .
    FLASH_OBProgramInitTypeDef OBInit;
    //First unlock the OB(Option Byte) memory access
    HAL_FLASH_OB_Unlock();
    //Lock back.
    HAL_FLASH_Lock();

    if (protectionValue == OB_RDP_LEVEL_0 )
    {
        OBInit.OptionType = OPTIONBYTE_RDP;
        OBInit.RDPLevel   = OB_RDP_LEVEL_0;
    }
    else if (protectionValue == OB_RDP_LEVEL_2 )
    {
        OBInit.OptionType = OPTIONBYTE_RDP;
        OBInit.RDPLevel   = OB_RDP_LEVEL_2;
    }
    else //(protectionValue == OB_RDP_LEVEL_1 )
    {
        OBInit.OptionType = OPTIONBYTE_RDP;
        OBInit.RDPLevel   = OB_RDP_LEVEL_1;
    }

    HAL_FLASHEx_OBProgram(&OBInit);

    /* Start the Option Bytes programming process */
    if (HAL_FLASH_OB_Launch() != HAL_OK)
    {

    }

    return true;
}


void commandMemWrite(uint8_t *data)
{
    uint32_t mem_address = 0;
    uint8_t write_status = 0x00;
    uint8_t payloadLength = 0;

    // 1) verify the checksum
    serialPutStr("BOOTLOADER_MSG: Memory Write\n");


    uint32_t hostCrc = createCrc(data[1], data[2], data[3], data[4]);


    uint8_t crcData[4] = {0};
    memcpy(crcData,data,1);
    if (!Bootloader_VerifyCrc(crcData,1,hostCrc))
    {
        mem_address = (uint32_t)((unsigned char)(data[5]) << 24 |
                                 (unsigned char)(data[6]) << 16 |
                                     (unsigned char)(data[7]) << 8 |
                                         (unsigned char)(data[8]));
        serialPutStr("BOOTLOADER_MSG: Checksum success !!\n");
				sprintf(BLOutput, "BOOTLOADER_MSG: Mem Write Address: %#x\n",mem_address);
				serialPutStr(BLOutput);

        if(bootloaderVerifyAddress(mem_address) == ADDR_VALID )
        {
            serialPutStr("BOOTLOADER_MSG: Valid Mem Write Address\n");

            payloadLength = data[9];
						sprintf(BLOutput, "BOOTLOADER_MSG: Payload Length: %d\n",payloadLength);
						serialPutStr(BLOutput);

            //execute mem write
            uint8_t writeData[4] = {0};
            memcpy(writeData,data+10,4);
            write_status = Bootloader_MemWrite(writeData,mem_address, payloadLength);
        }
        else
        {
            serialPutStr("BOOTLOADER_MSG: Invalid Mem Write Address.\n");
            write_status = ADDR_INVALID;
        }
    }
    else
    {
        serialPutStr("BOOTLOADER_MSG: Checksum fail !!\n");
        //checksum is wrong send nack
        Bootloader_SendNack();
    }
}

/*This function writes the contents of pBuffer to  "mem_address" byte by byte */
//Note1 : Currently this function supports writing to Flash only .
//Note2 : This functions does not check whether "mem_address" is a valid address of the flash range.
void commandFlashLock(uint8_t *data)
{
    serialPutStr("Flash Lock ACK\n");
    HAL_FLASH_Lock();
}

void commandFlashUnlock(uint8_t *data)
{
    serialPutStr("Flash Unlock ACK\n");
    HAL_FLASH_Unlock();
}

char MemWriteLog[500];
void commandMemWriteFile(uint8_t *data)
{
    uint32_t mem_address = 0;
    uint8_t write_status = 0x00;
    uint8_t payloadLength = 0;

    uint32_t hostCrc = createCrc(data[1], data[2], data[3], data[4]);


    uint8_t crcData[4] = {0};
    memcpy(crcData, data, 1);

    if (!Bootloader_VerifyCrc(crcData,1,hostCrc))
    {
        mem_address = (uint32_t)((unsigned char)(data[5]) << 24 |
                                 (unsigned char)(data[6]) << 16 |
                                     (unsigned char)(data[7]) << 8 |
                                         (unsigned char)(data[8]));

        if(bootloaderVerifyAddress(mem_address) == ADDR_VALID )
        {
            payloadLength = data[9];
            //Execute mem write
            uint8_t writeData[4] = {0};
            memcpy(writeData,data+10,4);
            write_status = Bootloader_MemWrite(writeData,mem_address, 4);
        }
        else
        {
            serialPutStr("BOOTLOADER_MSG: invalid mem write address\n");
            write_status = ADDR_INVALID;
        }
    }
    else
    {
        serialPutStr("BOOTLOADER_MSG:checksum fail !!\n");
        //checksum is wrong send nack
        Bootloader_SendNack();
    }
}



/*This function writes the contents of pBuffer to  "mem_address" byte by byte */
//Note1 : Currently this function supports writing to Flash only .
//Note2 : This functions does not check whether "mem_address" is a valid address of the flash range.
uint8_t Bootloader_MemWrite(uint8_t *pBuffer, uint32_t mem_address, uint32_t len)
{
    uint8_t status=HAL_OK;
    uint32_t data=0;

    data = (uint32_t)((unsigned char)(pBuffer[0]) << 24 |
                      (unsigned char)(pBuffer[1]) << 16 |
                          (unsigned char)(pBuffer[2]) << 8 |
                              (unsigned char)(pBuffer[3]));

    //We have to unlock flash module to get control of registers
    HAL_FLASH_Unlock();

    //Here we program the flash byte by byte
    status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD,mem_address,data );

    HAL_FLASH_Lock();

    return status;
}

uint8_t Bootloader_MemChunkWrite(uint8_t *pBuffer, uint32_t mem_address, uint32_t len)
{
    uint8_t status=HAL_OK;
    uint32_t data=0;

    for(int i=0; i<len; i+=4)
    {
        data = (uint32_t)((unsigned char)(pBuffer[i]) << 24 |
                          (unsigned char)(pBuffer[i+1]) << 16 |
                              (unsigned char)(pBuffer[i+2]) << 8 |
                                  (unsigned char)(pBuffer[i+3]));

        //We have to unlock flash module to get control of registers
        HAL_FLASH_Unlock();

        //Here we program the flash byte by byte
        status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD,mem_address,data );

        HAL_FLASH_Lock();

        mem_address += 1;
    }



    return status;
}

uint32_t execute_mem_read(uint32_t mem_address)
{
    HAL_FLASH_Unlock();
    uint32_t rdp_status=0;
    volatile uint32_t *pOB_addr = (uint32_t*) mem_address;
    rdp_status =  pOB_addr[0];
    HAL_FLASH_Lock();
    return rdp_status;
}



void commandMemRead(uint8_t *data)
{

    uint32_t mem_address = 0;
    uint32_t readData = 0;

    // 1) verify the checksum
    serialPutStr("BOOTLOADER_MSG:bootloader read memory \n");

    uint32_t hostCrc = createCrc(data[1], data[2], data[3], data[4]);

    uint8_t crcData[4] = {0};
    memcpy(crcData,data,1);
    if (!Bootloader_VerifyCrc(crcData,1,hostCrc))
    {
        mem_address = (uint32_t)((unsigned char)(data[5]) << 24 |
                                 (unsigned char)(data[6]) << 16 |
                                     (unsigned char)(data[7]) << 8 |
                                         (unsigned char)(data[8]));
        serialPutStr("BOOTLOADER_MSG: checksum success !!\n");

				sprintf(BLOutput,"BOOTLOADER_MSG: mem read address : %#x\n",mem_address);
				serialPutStr(BLOutput);

        if(bootloaderVerifyAddress(mem_address) == ADDR_VALID )
        {
            serialPutStr("BOOTLOADER_MSG: valid mem read address\n");

            readData = execute_mem_read(mem_address);

						sprintf(BLOutput,"BOOTLOADER_MSG: read data: 0x%x\n", readData);
						serialPutStr(BLOutput);
        }
        else
        {
            serialPutStr("BOOTLOADER_MSG: invalid mem read address\n");
        }
    }
    else
    {
        serialPutStr("BOOTLOADER_MSG:checksum fail !!\n");
        //checksum is wrong send nack
        Bootloader_SendNack();
    }
}


//This verifies the CRC of the given buffer in pData .
uint8_t Bootloader_VerifyCrc (uint8_t *pData, uint32_t len, uint32_t crc_host)
{
    uint32_t uwCRCValue=0;

    crc32(pData, len,&uwCRCValue);

    if( uwCRCValue == crc_host)
    {
        return VERIFY_CRC_SUCCESS;
    }

    return VERIFY_CRC_FAIL;
}

//Read the chip identifier or device Identifier
uint16_t Bootloader_GetMcuChipId(void)
{
    /*
    The STM32 MCUs integrate an MCU ID code. This ID identifies the ST MCU partnumber
    and the die revision. It is part of the DBG_MCU component and is mapped on the
    external PPB bus (see Section 33.16 on page 1304). This code is accessible using the
    JTAG debug pCat.2ort (4 to 5 pins) or the SW debug port (two pins) or by the user software.
    It is even accessible while the MCU is under system reset. */
    uint16_t cid;
    cid = (uint16_t)(DBGMCU->IDCODE) & 0x0FFF;
    return  cid;

}


uint8_t Bootloader_GetVersion(void)
{
    return (uint8_t)BL_VERSION;
}

/*This function sends NACK */
void Bootloader_SendNack(void)
{
    uint8_t nack = BL_NACK;
//    HAL_UART_Transmit(C_UART,&nack,1,HAL_MAX_DELAY);
    HAL_UART_Transmit(D_UART,&nack,1,HAL_MAX_DELAY);

}

#define APPLICATION_ADDRESS 0x0800C000U
void Bootloader_JumpToUserApp(void)
{
    HAL_FLASH_Unlock();
    uint32_t JumpAddress;
    pFunction Jump_To_Application;
    JumpAddress = *(__IO uint32_t*) (APPLICATION_ADDRESS + 4);
    Jump_To_Application = (pFunction) JumpAddress;
    JumpAddress = (*(__IO uint32_t*)APPLICATION_ADDRESS);
    if ( ( JumpAddress & 0x2FFE0000) == 0x20000000)//0x20000000
    {
        /* Jump to user application */
        JumpAddress = *(__IO uint32_t*) (APPLICATION_ADDRESS + 4);
        Jump_To_Application = (pFunction) JumpAddress;

        /* Initialize user application's Stack Pointer */
        __set_MSP(*(__IO uint32_t*) APPLICATION_ADDRESS);
        Jump_To_Application();
    }
}
