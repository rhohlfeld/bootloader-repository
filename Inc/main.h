/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f7xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
UART_HandleTypeDef *Get_DebugHandle(void);
UART_HandleTypeDef *Get_BleHandle(void);
/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define LED_OE_Pin GPIO_PIN_13
#define LED_OE_GPIO_Port GPIOC
#define SW_Util1_Pin GPIO_PIN_0
#define SW_Util1_GPIO_Port GPIOC
#define Main_PWR_Pin GPIO_PIN_2
#define Main_PWR_GPIO_Port GPIOC
#define ADC_Batt_Pin GPIO_PIN_3
#define ADC_Batt_GPIO_Port GPIOC
#define D_SW3_Dip_Switch_Pin GPIO_PIN_0
#define D_SW3_Dip_Switch_GPIO_Port GPIOA
#define VCC_A2_EN_PWR_Pin GPIO_PIN_1
#define VCC_A2_EN_PWR_GPIO_Port GPIOA
#define DEBUG_UART_TX_Pin GPIO_PIN_2
#define DEBUG_UART_TX_GPIO_Port GPIOA
#define DEBUG_UART_RX_Pin GPIO_PIN_3
#define DEBUG_UART_RX_GPIO_Port GPIOA
#define SW_Util2_Pin GPIO_PIN_4
#define SW_Util2_GPIO_Port GPIOA
#define ADC_dc_Prss_Pin GPIO_PIN_5
#define ADC_dc_Prss_GPIO_Port GPIOA
#define WIFI_RST_Pin GPIO_PIN_6
#define WIFI_RST_GPIO_Port GPIOA
#define ADC1_VCC_A2_Pin GPIO_PIN_7
#define ADC1_VCC_A2_GPIO_Port GPIOA
#define ADC_MTR_Pin GPIO_PIN_4
#define ADC_MTR_GPIO_Port GPIOC
#define SPI2_CS_Pin GPIO_PIN_1
#define SPI2_CS_GPIO_Port GPIOB
#define BL_LED_Pin GPIO_PIN_2
#define BL_LED_GPIO_Port GPIOB
#define LCD_I2C_SCL_Pin GPIO_PIN_10
#define LCD_I2C_SCL_GPIO_Port GPIOB
#define LCD_I2C_SDA_Pin GPIO_PIN_11
#define LCD_I2C_SDA_GPIO_Port GPIOB
#define WIFI_CE_Pin GPIO_PIN_12
#define WIFI_CE_GPIO_Port GPIOB
#define Disc_Pump_UART6TX_Pin GPIO_PIN_6
#define Disc_Pump_UART6TX_GPIO_Port GPIOC
#define Disc_Pump_UART6RX_Pin GPIO_PIN_7
#define Disc_Pump_UART6RX_GPIO_Port GPIOC
#define SD_CD_Pin GPIO_PIN_8
#define SD_CD_GPIO_Port GPIOC
#define LED_I2C_SDA_Pin GPIO_PIN_9
#define LED_I2C_SDA_GPIO_Port GPIOC
#define LED_I2C_SCL_Pin GPIO_PIN_8
#define LED_I2C_SCL_GPIO_Port GPIOA
#define BLE_UART_TX_Pin GPIO_PIN_9
#define BLE_UART_TX_GPIO_Port GPIOA
#define BLE_UART_RX_Pin GPIO_PIN_10
#define BLE_UART_RX_GPIO_Port GPIOA
#define SPI3_CS_Pin GPIO_PIN_15
#define SPI3_CS_GPIO_Port GPIOA
#define INT_1_Pin GPIO_PIN_2
#define INT_1_GPIO_Port GPIOD
#define VALVE_PWM_Pin GPIO_PIN_3
#define VALVE_PWM_GPIO_Port GPIOB
#define LED__RST_Pin GPIO_PIN_4
#define LED__RST_GPIO_Port GPIOB
#define PUMP_PWM_Pin GPIO_PIN_5
#define PUMP_PWM_GPIO_Port GPIOB
#define ADC_I2C_SCL_Pin GPIO_PIN_6
#define ADC_I2C_SCL_GPIO_Port GPIOB
#define ADC_I2C_SDA_Pin GPIO_PIN_7
#define ADC_I2C_SDA_GPIO_Port GPIOB
#define D_SW1_Dip_Switch_Pin GPIO_PIN_8
#define D_SW1_Dip_Switch_GPIO_Port GPIOB
#define D_SW2_Dip_Switch_Pin GPIO_PIN_9
#define D_SW2_Dip_Switch_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
