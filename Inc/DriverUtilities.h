#ifndef _UTILITIES_DRIVER_H
#define _UTILITIES_DRIVER_H
#include <stdint.h>
#include "main.h"

uint32_t DWT_Delay_Init(void);
void DWT_Delay_us(volatile uint32_t microseconds);

#endif