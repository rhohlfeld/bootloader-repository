#ifndef __BOOTLOADER_COMMANDS_H__
#define __BOOTLOADER_COMMANDS_H__

#include "main.h"
#include "Crc32.h"


#define FLASH_SECTOR2_BASE_ADDRESS 0x0800C000U
//version 1.0
#define BL_VERSION 0x10

#define BOOTLOADER_ENABLE    0xDEADBEEF
#define BOOTLOADER_DISENABLE 0xFFFFFFFF
#define TOTAL_FLASH_SECTORS		8


/* ACK and NACK bytes*/
#define BL_ACK   0XA5
#define BL_NACK  0X7F

/*CRC*/
#define VERIFY_CRC_FAIL    1
#define VERIFY_CRC_SUCCESS 0

#define ADDR_VALID 0x00
#define ADDR_INVALID 0x01

#define INVALID_SECTOR 0x04

/*Some Start and End addresses of different memories of STM32F446xx MCU */
/*Change this according to your MCU */
#define SRAM1_SIZE            240*1024     // STM32F7 has 112KB of SRAM1
#define SRAM1_END             (SRAM1_BASE + SRAM1_SIZE)
//#define SRAM2_SIZE            16*1024     // STM32F7 has 16KB of SRAM2
//#define SRAM2_END             (SRAM2_BASE + SRAM2_SIZE)
//#define FLASH_SIZE             1024*1024     // STM32F7 has 512KB of SRAM2
//#define BKPSRAM_SIZE           4*1024     // STM32F7 has 4KB of SRAM2
//#define BKPSRAM_END            (BKPSRAM_BASE + BKPSRAM_SIZE)

bool Gpio_ReadSwitch(void);
bool Gpio_ReadUtilSwitch1(void);
bool Gpio_ReadUtilSwitch2(void);
bool Gpio_StartSwitch(void);

bool Parse_Incoming_Command(uint8_t * buffer);
void printmsg(char *format,...);
void printmsgConsole(char *format,...);
void serialPutStr(const char *data);

void WriteToFlash(uint32_t data);
bool JumpToBootLoad(void);
void EnableBootLoader(void);
void Bootloader_JumpToUserApp(void);

uint8_t Bootloader_VerifyCrc(uint8_t *pData, uint32_t len, uint32_t crc_host);

uint8_t Bootloader_GetVersion(void);
uint16_t Bootloader_GetMcuChipId(void);

void Bootloader_SendAck(uint8_t command_code, uint8_t follow_len);
void Bootloader_SendNack(void);

uint8_t Bootloader_FlashErase(uint8_t sector_number, uint8_t number_of_sector);
uint8_t Bootloader_MemWrite(uint8_t *pBuffer, uint32_t mem_address, uint32_t len);
uint8_t Bootloader_MemChunkWrite(uint8_t *pBuffer, uint32_t mem_address, uint32_t len);

uint8_t FlashProtectionConfig(uint8_t sectorNum, uint8_t protectionLevel, uint8_t disable);
bool FlashSetSPRMOD(void);

uint16_t Bootloader_GetOptionBytesWRPLevel(void);
uint16_t Bootloader_GetOptionBytesRDPLevel(void);
bool Bootloader_GetSetOptionBytesRDPLevel(uint8_t protectionValue);


uint32_t execute_mem_read(uint32_t mem_address);


#endif
